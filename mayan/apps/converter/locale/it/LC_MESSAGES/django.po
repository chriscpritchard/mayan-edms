# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Marco Camplese <marco.camplese.mc@gmail.com>, 2016-2017
# Pierpaolo Baldan <pierpaolo.baldan@gmail.com>, 2011,2015
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-07 21:03-0400\n"
"PO-Revision-Date: 2017-09-23 21:29+0000\n"
"Last-Translator: Marco Camplese <marco.camplese.mc@gmail.com>\n"
"Language-Team: Italian (http://www.transifex.com/rosarior/mayan-edms/language/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:20 permissions.py:7 settings.py:12
msgid "Converter"
msgstr "Convertitore"

#: apps.py:27 models.py:39
msgid "Order"
msgstr "Ordine"

#: apps.py:29 models.py:57
msgid "Transformation"
msgstr "Trasformazione"

#: apps.py:33 models.py:49
msgid "Arguments"
msgstr "Argomenti"

#: backends/python.py:176 backends/python.py:182
#, python-format
msgid "Exception determining PDF page count; %s"
msgstr "Eccezione che determina il conteggio pagine PDF: %s"

#: backends/python.py:196
#, python-format
msgid "Exception determining page count using Pillow; %s"
msgstr "Eccezione sollevata dal conteggio pagine usando Pillow: %s"

#: classes.py:98
msgid "Not an office file format."
msgstr "Non è un formato di file office"

#: classes.py:119
msgid "LibreOffice not installed or not found."
msgstr "LibreOffice non installato o non trovato."

#: links.py:35
msgid "Create new transformation"
msgstr "Crea nuova trasformazione"

#: links.py:39
msgid "Delete"
msgstr "Cancella"

#: links.py:43
msgid "Edit"
msgstr "Modifica"

#: links.py:47 models.py:58
msgid "Transformations"
msgstr "Trasformazioni"

#: models.py:37
msgid ""
"Order in which the transformations will be executed. If left unchanged, an "
"automatic order value will be assigned."
msgstr "Ordine delle trasformazioni da eseguire. Se resta invariato verrà assegnato l'ordine automatico."

#: models.py:43
msgid "Name"
msgstr "Nome "

#: models.py:47
msgid ""
"Enter the arguments for the transformation as a YAML dictionary. ie: "
"{\"degrees\": 180}"
msgstr "Scrivi gli argomenti per la trasformazione come dizionario YAML. es: {\"degrees\": 180}"

#: permissions.py:10
msgid "Create new transformations"
msgstr "Crea una nuove trasformazioni"

#: permissions.py:13
msgid "Delete transformations"
msgstr "Cancella le trasformazioni"

#: permissions.py:16
msgid "Edit transformations"
msgstr "Modifica le trasformazioni"

#: permissions.py:19
msgid "View existing transformations"
msgstr "Visualizza le trasformazioni esistenti"

#: settings.py:15
msgid "Graphics conversion backend to use."
msgstr "Conversioni grafiche di backend da utilizzare."

#: settings.py:34
msgid "Configuration options for the graphics conversion backend."
msgstr "Opzioni di configurazione per il backend di conversione grafica."

#: transformations.py:85
msgid "Crop"
msgstr "Taglia"

#: transformations.py:98
msgid "Flip"
msgstr "Capovolgi"

#: transformations.py:109
msgid "Gaussian blur"
msgstr "Sfocatura gaussiana"

#: transformations.py:119
msgid "Line art"
msgstr "Line art"

#: transformations.py:130
msgid "Mirror"
msgstr "Specchio"

#: transformations.py:141
msgid "Resize"
msgstr "Ridimensiona"

#: transformations.py:168
msgid "Rotate"
msgstr "Ruotare"

#: transformations.py:187
msgid "Rotate 90 degrees"
msgstr "Ruota di 90 Gradi"

#: transformations.py:198
msgid "Rotate 180 degrees"
msgstr "Ruota di 180 gradi"

#: transformations.py:209
msgid "Rotate 270 degrees"
msgstr "Ruota di 270 gradi"

#: transformations.py:219
msgid "Unsharp masking"
msgstr "Maschera di contrasto"

#: transformations.py:235
msgid "Zoom"
msgstr "Zoom"

#: validators.py:21
msgid "Enter a valid YAML value."
msgstr "Inserisci un valore YAML valido."

#: views.py:64
#, python-format
msgid "Delete transformation \"%(transformation)s\" for: %(content_object)s?"
msgstr "Cancellare le trasformazioni \"%(transformation)s\" per: %(content_object)s?"

#: views.py:116
#, python-format
msgid "Create new transformation for: %s"
msgstr "Crea una nuove trasformazioni per: %s"

#: views.py:166
#, python-format
msgid "Edit transformation \"%(transformation)s\" for: %(content_object)s"
msgstr "Modifica le trasformazioni \"%(transformation)s\" per: %(content_object)s"

#: views.py:213
#, python-format
msgid "Transformations for: %s"
msgstr "Trasformazione per: %s"
